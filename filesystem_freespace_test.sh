#! /bin/bash

# Copyright (C) 2013 Charles Atkinson
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA

# Purpose: 
#   * Tests perfomance of file system at increasing levels of space usage.
#     Notes:
#     1. "Space usage" is what df lists as Use%.
#     2.  Use% is "used/(used+free)", not the intuitively obvious "used/size".
#   * Tests the perfomance of the underlying device, regardless of the file
#     system (hdparm -tT).
#     This is intended to give a baseline read-from-device figure, bypassing
#     the Linux file system cache for comparison with later read tests.
#   * Writes files until usage exceeds a configurable threshold by not more
#     than 1%.
#     This is intended to simulate typical usage.
#     Notes:
#     1. The files are filled with zeroes and have sizes randomly chosen
#        from a configurable range.
#     2. The random size choice is weighted to create more smaller files than
#        larger ones, using a e^(k*(x-1)) function where k is a configurable
#        constant and x is a random number between 0 and 1.
#        Thanks to Kaushik for the weighting function.
#   * For every ten files created:
#     1. The file system's and device's buffers are flushed (hdparm -f -F).
#     2. A file chosen at random is deleted.
#     3. The file system's and device's buffers are flushed again.
#     This is intended to simulate typical usage and to produce fragmentation.
#   * When the usage exceeds the configurable threshold, timed performance
#     tests are run, as initialised in the initialise function, and
#     test results  are written to the report file.  The report file is intended
#     for use by gnuplot.  Data is subsequently parsed out of its comment lines
#     by the write_gnuplot_scripts function.
#   * When the tests are complete increment the file system usage by a
#     configurable percentage unless the configurable maximum percentage would
#     be exeeded.  If the usage is incremented, repeat the tests.
#   * Write a gnuplot script file for each test
#     

# Usage: 
#   * General: see usage function below (search for "function usage") or use
#     -h option.
#   * In case of SATA hard drives which show poor performance with sequential
#     reads (e.g. hdparm -t), from 20 to 90% of the expected value, it may be
#     an AHCI/NCQ firmware bug.  Information at:
#     https://ata.wiki.kernel.org/index.php/Known_issues#Problem_description_4
    
# Versions:
#   * 0.0 Developed and tested with:
#     - Slackware64 14.0
#     - bash 4.2.37
#     - bc 1.06.95
#     - file system on partitions on HDDs on SATA 6 GB/s port

# History:
#   Version 0.0, 06 Aug 2013, Charles
#   * Creation
script_ver='0.0'

# Programmers' notes: error and trap handling: 
#   * All errors are fatal and finalise() is called.
#   * At any time, a trapped event may transfer control to finalise().
#   * To allow finalise() to tidy up before exiting, changes that need to be
#     undone are noted with global variables named <change name>_flag and the
#     data required to undo those changes is kept in global variables.
#   * finalise() uses the same functions to undo the changes as are used when
#     they are undone routinely.
#   * $finalising_flag is used to prevent recursive calls when errors are
#     encountered while finalise() is running,

# Programmers' notes: variable names and values
#   * Directory names: *_dir.  Their values should not have a trailing /
#   * File names: *_fn
#   * Logicals: *_flag containing values $true or $false.
#     The $true and $false values allow "if [[ $debugging_flag ]]"
#   * $buf is a localised scratch buffer

# Programmers' notes: global variables
#   * debugging_flag  Set to $true when -d option given
#   * false  One of two values assigned to logic (*_flag) variables.  An empty string so tests as false
#   * file_size_max  The maximum size of file to create when filling the file system to a Use% level
#   * file_size_min  The minimum size of file to create when filling the file system to a Use% level
#   * file_system_dev  The /dev device file corresponding to the file system mounted at $mountpoint, for example /dev/sda3
#   * finalising_flag  Set to $true when the finalising function has been called
#   * fs_avail_bytes  Number of bytes of the file system available (from df)
#   * fs_blocksize  The file system blocksize in bytes, for example 4096
#   * fs_data  Key data about the file system, for the report
#   * fs_inode_size  The file system inode size in bytes, for example 256
#   * fs_type  The file system type, for example ext4
#   * fs_used_bytes  Number of bytes of the file system used (from df)
#   * fs_used_percent  Percentage of the file system used (calculated from df used and avail)
#   * global_error_flag  Set to $true when an error message has been generated by this script
#   * global_warning_flag  Set to $true when a warning message has been generated by this script
#   * hdd_dev  The /dev device file for the HDD containing the file system mounted at $mountpoint, for example /dev/sda
#   * hdd_dev_data  Key data about hdd_dev, for the report
#   * k  The constant used in the random size choice weighting function
#   * mountpoint  The mount point of the file system to be tested
#   * output_dir  The directory to write the output files in
#   * output_fn_timestamp  The timestamp to include in output file names (output of date +%d-%b-%Y@%R)
#   * pid_dir  The PID file's directory, /var/run
#   * pid_fn  The PID file, used for ensuring only one instance of this script is running
#   * report_fn  The file system performance report
#   * script_name  The name byu which this script was called, stripped of any leading path components
#   * target_use_percent  The target Use% to wwhich this script fills the file system.  Incremented by the script
#   * target_use_percent_increment  The increment by which this script increments $target_use_percent
#   * target_use_percent_max  The maximum value of Use% that this script will increment $target_use_percent to
#   * target_use_percent_max_max  The internal limit on $target_use_percent_max
#   * test_repeat_count  Number of times to repeat each test
#   * tmp_dir  Temporary directory, created by this script
#   * tmp_dir_created_flag  Set to $true when the temprary directory has been created by this script
#   * true  One of two values assigned to logic (*_flag) variables.  Has the value "true" so tests as true.

# Programmers' notes: function call tree
#    +
#    |
#    +-- initialise
#    |   |
#    |   +-- usage
#    |   |
#    |   +-- initialise_report
#    |   |
#    |   +-- get_file_system_data
#    |   |   |
#    |   |   +-- align_data
#    |   |
#    |   +-- do_pid
#    |
#    +-- run_tests
#    |   |
#    |   +-- test_raw_sequential_read
#    |   |
#    |   +-- fill_to_level
#    |   |   |
#    |   |   +-- choose_file_size
#    |
#    +-- write_gnuplot_scripts
#    |   |
#    |   +-- get_y_max
#    |
#    +-- finalise
#
# Utility functions called from various places:
#     calculate ck_file fct flush_buffers get_call_stack get_fs_usage msg write_to_report

# Function definitions in alphabetical order.  Execution begins after the last function definition.

#--------------------------
# Name: align_data
# Purpose:
#   Aligns ":" characters in lines in the argument string by inserting spaces
#   before the ":"s
# Arguments:
#   $1 the position to align
#   $2 the string to align
# Global variable usage: none
# Outputs: writes the aligned string to stdout
#--------------------------
function align_data {
    local buf= col_max=$1 line

    while read -r line
    do
        buf=$buf$'\n'$(printf "%-${col_max}s: %s" "${line%%:*}" "${line#*: }")
    done < <(echo "$2") 
    echo "${buf#$'\n'}"

}  # end of function align_data

#--------------------------
# Name: calculate
# Purpose:
#   Does an arithmetic calculation using bc
# Arguments:
#   $1 Arithmetic expression(s) string
#   $2 Scale (in bc terminology); number of decimal places to use in calculation
#   $3 Rounding control
#       * Must end in DP (decimal places) or SF (significant figures).
#       * 0DP can be used for rounding to the nearest integer.
#       * Scale ($1) must be at least 2 greater than any *DP value unless it's 0DP.
# Global variables read: regex_uint
# Global variables set: none
# Outputs: writes the result to stdout
# Returns: always returns zero; does not return on error
#--------------------------
function calculate {
    # TODO: adapt for use in arithmetic comparisons as well as calculations
    local buf dp_flag emsg expression regex rounding scale sf_flag

    # Pre-calculation error traps
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~
    emsg=

    expression=${1:-}
    [[ $expression = '' ]] && emsg=$emsg$'\n'"Expression (\$1) is empty"

    scale=${2:-}
    [[ ! $scale =~ $regex_uint ]] && emsg=$emsg$'\n'"Scale (\$2, '$scale') is not an unsigned integer"

    rounding=${3:-}
    regex='(DP)|(SF)$'
    if [[ $rounding =~ $regex ]]; then
        if [[ $rounding =~ SF$ ]]; then
            sf_flag=$true
            dp_flag=$false
        else
            sf_flag=$false
            dp_flag=$true
        fi
        rounding=${rounding:0:${#rounding}-2}
        [[ $rounding != '' && ! $rounding =~ $regex_uint ]] \
            && emsg=$emsg$'\n'"Rounding numeric part (\$3, '$rounding') is not an unsigned integer"
    else
        emsg=$emsg$'\n'"Rounding (\$3, '$rounding') does not end in SF or DP"
    fi
    ((rounding>0)) \
        && ((scale<(rounding+2))) \
        && emsg=$emsg$'\n'"Scale (\$1, '$scale') must be at least two greater than the rounding numeric value (\$3, '$rounding')"
    [[ $emsg != '' ]] && msg E "Programming error:$emsg"$'\n  Call stack trace:\n'"$(get_call_stack)"

    # Calculation
    # ~~~~~~~~~~~     
    buf=$(echo "scale=$scale; $expression" | bc 2>&1)
    regex='syntax error'
    [[ $buf =~ $regex ]] && msg E $'Programming error: calculate called with invalid argument(s)\n'"  Scale: $scale"$'\n'"  Expression: $expression"$'\n  Call stack trace:\n'"$(get_call_stack)"

    # Rounding
    # ~~~~~~~~
    if [[ $sf_flag ]]; then
        printf -v buf "%.$rounding"g "$buf"
    else
        printf -v buf "%.$rounding"f "$buf"
    fi

    echo "$buf"

}  # end of function calculate

#--------------------------
# Name: choose_file_size
# Purpose: randomly chooses a file size, subject to not exceeding the
#   required file system usage by 1%.
# Global variable usage:
#   * Reads:
#     - file_size_min
#     - file_size_range_size
#     - k
#     - mountpoint
#     - target_use_percent
#   * Sets: file_size
#--------------------------
function choose_file_size {
    local available_blocks buf current_available_bytes current_used_bytes next_use_percent next_used_bytes used_blocks

    # Get the number of file system bytes already used and available
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    buf=$(df --block-size=1K "$mountpoint" 2>&1)
    (($?>0)) && msg E "Unable to list file system size: $buf"
    read _ _ _ _ _ _ _ _ _ used_blocks available_blocks _ < <( echo $buf ) 
    fs_avail_bytes=$(calculate "$available_blocks*1024" 0 0DP)
    current_used_bytes=$(calculate "$used_blocks*1024" 0 0DP)
    #msg D "current_used_bytes: $current_used_bytes, fs_avail_bytes: $fs_avail_bytes"

    # Choose a size
    # ~~~~~~~~~~~~~
    while true
    do
        file_size=$(
            printf %0.f $(
                echo "$file_size_min+($file_size_range_size*(e($k*(($RANDOM/32767)-1))))" \
                    | bc -l
            )
        )
        next_used_bytes="($file_size + $current_used_bytes)"
        next_available_bytes="($fs_avail_bytes - $file_size )"
        next_use_percent="((100 * $next_used_bytes) / ($next_used_bytes + $next_available_bytes ))"
        (($(echo "$next_use_percent < $target_use_percent" | bc -l)==1)) && break
   done
}  # end of function choose_file_size

#--------------------------
# Name: ck_file
# Version: 1.0
# Purpose: for each file listed in the argument list: checks that it is 
#   * reachable and exists
#   * is of the type specified (block special, ordinary file or directory)
#   * has the requested permission(s) for the user
#   * optionally, is absolute (begins with /)
# Usage: ck_file [ path <file_type>:<permissions>:[a] ] ...
#   where 
#     file  is a file name (path)
#     file_type  is b (block special file), f (file) or d (directory)
#     permissions  is none or more of r, w and x
#     a  requests an absoluteness test (that the path begins with /)
#   Example: ck_file foo d:rwx:
# Outputs:
#   * For the first requested property each file does not have, a message to
#     stderr
#   * For the first detected programminng error, a message to
#     stderr
# Returns: 
#   0 when all files have the requested properties
#   1 when at least one of the files have the requested properties
#   2 when a programming error is detected
#--------------------------
function ck_file {

    local absolute_flag buf file_name file_type perm perms retval

    # For each file ...
    # ~~~~~~~~~~~~~~~~~
    retval=0
    while [[ $# -gt 0 ]]
    do  
        file_name=$1
        file_type=${2%%:*}
        buf=${2#$file_type:}
        perms=${buf%%:*}
        absolute=${buf#$perms:}
        case $absolute in 
            '' | a )
                ;;
            * )
                echo "ck_file: invalid absoluteness flag in '$2' specified for file '$file_name'" >&2
                return 2
        esac
        shift 2

        # Is the file reachable and does it exist?
        # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        case $file_type in
            b ) 
                if [[ ! -b $file_name ]]; then
                    echo "file '$file_name' is unreachable, does not exist or is not a block special file" >&2
                    retval=1
                    continue
                fi  
                ;;  
            f ) 
                if [[ ! -f $file_name ]]; then
                    echo "file '$file_name' is unreachable, does not exist or is not an ordinary file" >&2
                    retval=1
                    continue
                fi  
                ;;  
            d ) 
                if [[ ! -d $file_name ]]; then
                    echo "directory '$file_name' is unreachable, does not exist or is not a directory" >&2
                    retval=1
                    continue
                fi
                ;;
            * )
                echo "$my_nam: ck_file: invalid file type '$file_type' specified for file '$file_name'" >&2
                return 2
        esac

        # Does the file have the requested permissions?
        # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        buf="$perms"
        while [[ $buf ]]
        do
            perm="${buf:0:1}"
            buf="${buf:1}"
            case $perm in
                r )
                    if [[ ! -r $file_name ]]; then
                        echo "$file_name: no read permission" >&2
                        retval=1
                        continue
                    fi
                    ;;
                w )
                    if [[ ! -w $file_name ]]; then
                        echo "$file_name: no write permission" >&2
                        retval=1
                        continue
                    fi
                    ;;
                x )
                    if [[ ! -x $file_name ]]; then
                        echo "$file_name: no execute permission" >&2
                        retval=1
                        continue
                    fi
                    ;;
                * )
                    echo "ck_file: invalid permisssion '$perm' requested for file '$file_name'" >&2
                    return 2
            esac
        done

        # Does the file have the requested absoluteness?
        # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        if [[ $absolute = a && ${file_name:0:1} != / ]]; then
            echo "$file_name: does not begin with /" >&2
            retval=1
        fi

    done

    return $retval

}  #  end of function ck_file

#--------------------------
# Name: do_pid
# Version: 0.1
# Purpose:
#   * If the PID file exists
#         If the PID file is locked
#             Exit
#         Else
#             Refresh it and take a lock on it
#     Else
#         Create it and lock it
# Arguments:
#    $1 - the script's argument list
# Outputs: none except via msg()
# Global variables:
#    pid_dir: read
#    pid_fn: set
# Returns: 
#   Does not return if there is a locked PID file.
#   Otherwise returns 0
# Usage notes:
#   * Caller should ensure the PID directory exists and has rwx permissions
#--------------------------
function do_pid {
    fct "${FUNCNAME[0]}" 'started'
    
    pid_fn=$pid_dir/$script_name.pid

    exec 9>"$pid_fn"
    if flock --exclusive 9; then
        pid_file_created_flag=$true
        msg D "Taken lock on PID file $pid_fn"
    else
        msg E "Another instance is running.  Contents of $pid_fn: $(< "$pid_fn")"
    fi
    
    fct "${FUNCNAME[0]}" 'returning'
}  #  end of function do_pid

#--------------------------
# Name: fct
# Purpose: function call trace (for debugging)
# $1 - name of calling function 
# $2 - message.  If it starts with "started" or "returning" then the output is prettily indented
#--------------------------
function fct {

    if [[ ! $debugging_flag ]]; then
        return 0
    fi

    fct_ident="${fct_indent:=}"

    case $2 in
        'started'* )
            fct_indent="$fct_indent  "
            msg D "$fct_indent$1: $2"
            ;;
        'returning'* )
            msg D "$fct_indent$1: $2"
            fct_indent="${fct_indent#  }"
            ;;
        * )
            msg I "DEBUG: $fct_indent$1: $2"
    esac

}  # end of function fct

#--------------------------
# Name: fill_to_level
# Purpose: fills the file system up to the target use%
# Return: always 0; does not return on error
#--------------------------
function fill_to_level {
    #fct "${FUNCNAME[0]}" 'started'
    local buf fn i old_dir random remove_fn

    msg I "File system usage before filling to $target_use_percent: $(get_fs_usage)%"

    # Create files until the target Use% is achieved
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    fn=0
    i=0
    old_dir=$PWD
    cd "$tmp_dir" 2>/dev/null
    (($?>0)) && msg E "Unable to cd: $(cd "$tmp_dir" 2>&1)"
    while (($(echo "scale=$bc_scale; $(get_fs_usage) < ($target_use_percent)" | bc)))
    do
        # Create a file
        # ~~~~~~~~~~~~~
        choose_file_size
        while [[ -f "$fn" ]]    # Find a free file name
        do
            fn=$(echo "scale=0; $fn + 1" | bc )
        done
        #msg D "Creating file $fn with size $(printf "%'d" $file_size) bytes"
        if ((file_size>0)); then
            buf=$(dd if=/dev/zero of=$fn bs=$file_size count=1 2>&1)
            rc=$?
        else
            buf=$(touch "$fn" 2>&1)
            rc=$?
        fi
        if ((rc>0)); then
            msg E "Unable to create file: $buf"
        fi
    
        # Remove a file every 10th iteration
        # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        # TODO: make this a configurable number
        if ((++i==10)); then 
            i=0
            # Choose a file randomly within the range 0 to $fn
            random=$RANDOM
            if ((random>0)); then
                remove_fn=$(printf %0.f $(echo "$fn * ($random / 32767)" | bc -l))
            else
                remove_fn=0
            fi
            # While the candidate file does not exist, increment its name
            while [[ ! -f $remove_fn ]]    
            do
                remove_fn=$(echo "scale=0; $remove_fn + 1" | bc)
                [[ $remove_fn = $fn ]] && remove_fn=0    # Got to the last file -- go to the first
            done
            #msg D "Removing file $remove_fn"
            rm "$remove_fn"
            flush_buffers
        fi

        # Progress marker
        # ~~~~~~~~~~~~~~~
        [[ ! $debugging_flag ]] && echo -n .
    done
    cd "$old_dir" 2>/dev/null
    (($?>0)) && msg E "Unable to cd: $(cd "$old_dir" 2>&1)"

    # TODO: report how many files in 0-10 bits, 11-100, 101-1000 ... sizes
    msg I "File system usage after filling to $target_use_percent: $(get_fs_usage)%"
    #fct "${FUNCNAME[0]}" 'returning'
}  # end of function fill_to_level

#--------------------------
# Name: finalise
# Purpose: cleans up and gets out of here
# Arguments: $1: return value to exit with
#--------------------------
function finalise {
    fct "${FUNCNAME[0]}" 'started'
    local buf msgs my_retval

    finalising_flag=$true
    
    # Set return value
    # ~~~~~~~~~~~~~~~~
    # Choose the highest and give message if finalising on a trapped signal
    my_retval=$1
    case $my_retval in 
        129 | 130 | 131 | 143 )
            case $my_retval in
                129 )
                    buf='SIGHUP'
                    ;;
                130 )
                    buf='SIGINT'
                    ;;
                131 )
                    buf='SIGQUIT'
                    ;;
                143 )
                    buf='SIGTERM'
                    ;;
            esac
            msg I "finalising on $buf"
            ;;
    esac

    # Clean up
    # ~~~~~~~~
    [[ $pid_file_created_flag ]] && rm "$pid_fn" 2>/dev/null
    #msg I "DEVEL: not removing temporary directory"
    [[ $tmp_dir_created_flag && ${tmp_dir:-} != '' && -d $tmp_dir ]] && rm -fr "$tmp_dir"

    # Final log messages
    # ~~~~~~~~~~~~~~~~~~
    msgs=
    if [[ $global_warning_flag ]]; then
        msgs="$msgs"$'\n'"There was one or more WARNINGs"
        ((my_retval == 0)) && my_retval=1
    fi
    if [[ $global_error_flag ]]; then
        msgs="$msgs"$'\n'"There was one or more ERRORs"
        ((my_retval == 0)) && my_retval=1
    fi
    if [[ "$msgs" != '' ]]; then
        msgs="${msgs#$'\n'}"        # strip leading linefeed
        msg I "$msgs"
    fi
    msg I "Exiting with return value $my_retval"

    # Exit
    # ~~~~
    # In case this shell/process was called by this script by $(<function name> ...)
    # (to pass a string back to caller), the parent would continue to execute after
    # this shell/process exits.
    # I tried using trap with SIGUSR1 but did not get it to work (only tried sending 
    # to the $$ process -- not to each process' parent).
    # The solution was to send $$ SIGKILL.
    # It's not nice; it sets the exit code to 137 and prints Killed.
    # TODO: try to find a nicer way.  Try sending USR1 to the whole process group.
    fct "${FUNCNAME[0]}" 'exiting'
    #(($BASHPID!=$$)) && kill -s USR1 $$
    (($BASHPID!=$$)) && kill -9 $$
    exit $my_retval
}  # end of function finalise

#--------------------------
# Name: flush_buffers
# Purpose: flushes the OS file system buffer and, if the HDD implements it,
#   the HDD's write cache buffer
#--------------------------
function flush_buffers {
    #fct "${FUNCNAME[0]}" 'started'
    local buf

    ((EUID!=0)) && return    # Need to be root to flush buffers

    # Sync and flush the buffer cache
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # Use hdparm -f rather than blockdev --flushbufs because it runs the same 
    # flushing system call plus five others.
    buf=$(hdparm -f "$hdd_dev" 2>&1)
    rc=$?
    ((rc>0)) && msg E "Unable to flush the buffer cache: $buf"

    # Free cached dentries and inodes
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # Required in case the above keeps a copy in memory which reads could use.
    # Normally preceded by calling sync but hdparm -f calls sync
    # References:
    # * https://www.kernel.org/doc/Documentation/sysctl/vm.txt
    # * http://catalin-festila.blogspot.in/2011/06/myth-of-dropcaches.html
    echo 2 > /proc/sys/vm/drop_caches
    
    # Flush the on-drive write cache buffer
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    buf=$(hdparm -F "$hdd_dev" 2>&1)
    rc=$?
    ((rc>0)) && msg E "Unable to flush the on-drive write cache buffer: $buf"

    #msg D "${FUNCNAME[0]}: Key /proc/meminfo values: $(grep -E '^Cached:|^Dirty:' /proc/meminfo)"

    #fct "${FUNCNAME[0]}" 'returning'
}  # end of function flush_buffers

#--------------------------
# Name: get_call_stack
# Purpose: writes a call stack trace to stdout
# Acknowledgement: thanks to Yu-Jie Lin
#   * http://blog.yjl.im/2012/01/printing-out-call-stack-in-bash.html
#--------------------------
function get_call_stack {
    local i start

    # For each function called
    # ~~~~~~~~~~~~~~~~~~~~~~~~
    # Starting with main and not showing the call to this function
    start=$((${#BASH_LINENO[@]}-2))
    for ((i=start;i>0;i--))
    do
        # Show the line number, which function it is in and the line itself
        echo "    Line ${BASH_LINENO[i]}, in ${FUNCNAME[i+1]}"
        sed -n "${BASH_LINENO[i]}{s/^[[:space:]]*/      /;p}" "${BASH_SOURCE[i+1]}"
    done

}  # end of function get_call_stack

#--------------------------
# Name: get_file_system_data
# Purpose: gets invariant data about the file system
# Global variable usage:
#   * Reads:
#     - mountpoint
#   * Sets:
#     - file_system_dev
#     - fs_data
#     - hdd_dev
#     - hdd_dev_data
#--------------------------
function get_file_system_data {
    fct "${FUNCNAME[0]}" 'started'
    local buf col_max end_sector fdisk_out fs_size_bytes fs_size_MiB fs_size_GiB mount_options regex start_sector sys_block total_sectors 

    # Get the device files
    # ~~~~~~~~~~~~~~~~~~~~
    buf=$(df "$mountpoint" 2>&1)
    read _ _ _ _ _ _ _ file_system_dev _ < <( echo $buf )
    regex=^/dev/
    if [[ ! $file_system_dev =~ $regex ]]; then
        msg E "File system device '$file_system_dev' does not begin with /dev/ (parsed from df output $buf)"
    fi
    regex=^/dev/sd[a-z]+[1-9][0-9]?$
    if [[ ! $file_system_dev =~ $regex ]]; then
        msg E "The file system device (df's \"Filesystem\") does not match regex $regex" 
    fi
    hdd_dev=${file_system_dev%%*([0-9])}

    # Get the file system data
    # ~~~~~~~~~~~~~~~~~~~~~~~~
    sys_block="/sys/block/${hdd_dev##*/}/${file_system_dev##*/}/size"
    buf=$(cat "$sys_block" 2>&1)
    (($?>0)) && msg E "cat $sys_block output: $buf"
    fs_size=$(echo "$buf * 512" | bc)
    read _ _ fs_type mount_options _ < <( grep "^$file_system_dev " /proc/mounts) 

    # Format the file system data
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~
    for unit in bytes KiB MiB GiB TiB PiB EiB ZiB YiB
    do
        (($(echo "$fs_size < 1024" | bc))) && break
        fs_size=$(calculate "$fs_size/1024" 4 2DP)
    done
    case $fs_type in
        ext4 )
            buf=$(tune2fs -l $file_system_dev 2>&1)
            (($?>0)) && msg E "Unable to get file system data: $buf" 
            fs_inode_size=$(echo "$buf" | grep '^Inode size:' | awk "{print \$3}")
            fs_blocksize=$(echo "$buf" | grep '^Block size:' | awk "{print \$3}")
            ;;
        * )
            msg E "Code not written for file system type $fs_type"
    esac
    fs_data="Type: $fs_type
Inode size: $fs_inode_size
Blocksize: $fs_blocksize"

    # Get the partition data
    # ~~~~~~~~~~~~~~~~~~~~~~
    fdisk_out=$(fdisk -l "$hdd_dev" 2>&1)
    (($?>0)) && msg E "fdisk -l output: $fdisk_out"
    fdisk_out=$(echo -e "$fdisk_out" | sed 's/\*//')    # Remove any Boot column *
    read _ start_sector end_sector _ < <(echo "$fdisk_out" | grep "^$file_system_dev ")
    read _ _ _ _ _ _ _ total_sectors _ < <(echo "$fdisk_out" | grep ' sectors$')
    start_sector=$(calculate "(($start_sector+1)*100)/$total_sectors" 4 2DP)
    end_sector=$(calculate "(($end_sector+1)*100)/$total_sectors" 4 2DP)

    # Format the partition data
    # ~~~~~~~~~~~~~~~~~~~~~~~~~
    part_data="Partition size: $(printf "%'.2f $unit" "$fs_size")
Position on device: $start_sector% to $end_sector%"

    # Get the device data
    # ~~~~~~~~~~~~~~~~~~~
    buf=$(hdparm -I "$hdd_dev" 2>&1)
    (($?>0)) && msg E "hdparm -I output: $buf"
    buf=$(echo "$buf" \
        | grep -E 'Model Number:|Serial Number:|Firmware Revision:|Transport:|Logical/Physical Sector size:|Nominal Media Rotation Rate:'
    )
    hdd_dev_data=$(echo "$buf" | sed -e 's/^\t//' -e 's/  \+/ /g' -e 's/ *$//')
    buf=$(echo "$fdisk_out" | grep '^Disk .* bytes$')
    # TODO: format the size value in bytes, Mib and GiB for consistency with $fs_data (create a function)
    hdd_dev_data=$hdd_dev_data$'\n'"Size:${buf#*:}"

    # Format the data
    # ~~~~~~~~~~~~~~~
    col_max=0
    while read -r line
    do
        buf=${line%%:*}
        len=${#buf}
        ((len>col_max)) && col_max=$len
    done < <(echo "fs_data"$'\n'"$part_data"$'\n'"$hdd_dev_data") 
    fs_data=$(align_data $col_max "$fs_data")
    part_data=$(align_data $col_max "$part_data")
    hdd_dev_data=$(align_data $col_max "$hdd_dev_data")

    msg I "File system data:"$'\n'"$fs_data"
    msg I "Partition data:"$'\n'"$part_data"
    msg I "Storage device data:"$'\n'"$hdd_dev_data"

    fct "${FUNCNAME[0]}" 'returning'
}  # end of function get_file_system_data

#--------------------------
# Name: get_fs_usage
# Purpose: gets the file system usage
# Outputs: Use% written to stdout
# Global variables:
#   - fs_avail_bytes: set
#   - fs_used_bytes: set
#   - fs_used_percent: set (and written to stdout)
#--------------------------
function get_fs_usage {
    local available_blocks buf free_inodes used_inodes_percent used_blocks used_inodes

    # Get the number of file system bytes available and used
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    buf=$(df --block-size=1K "$mountpoint" 2>&1)
    (($?>0)) && msg E "Unable to list file system size: $buf"
    read _ _ _ _ _ _ _ _ _ used_blocks available_blocks _ < <( echo $buf ) 
    #msg D "used_blocks: $used_blocks, available_blocks: $available_blocks"
    fs_used_bytes=$(echo "$used_blocks*1024" | bc)
    fs_avail_bytes=$(echo "$available_blocks*1024" | bc)
    # Next line not required now tests ensure there's enough space first
    # (and we don't want to abort the script after a possibly very log run without producing plot scripts)
    #((fs_avail_bytes==0)) && msg E "File system filled; data unreliable; aborting tests"
    #msg D "fs_used_bytes: $fs_used_bytes, fs_avail_bytes: $fs_avail_bytes"

    # Sanity check the inode usage
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    buf=$(df --inodes "$mountpoint" 2>&1)
    (($?>0)) && msg E "Unable to list file system inode statistics: $buf"
    read _ _ _ _ _ _ _ _ _ used_inodes free_inodes _ < <( echo $buf ) 
    #msg D "used_blocks: $used_blocks, available_blocks: $available_blocks"
    used_inodes_percent=$(calculate "(100*$used_inodes)/($used_inodes+$free_inodes)" 4 2DP)
    if (($(echo "scale=4; $used_inodes_percent>99" | bc ))); then
        msg W "inode usage is $used_inodes_percent.  Setting fs_used_percent to 0"
        fs_used_percent=0
    fi

    # Calculate the Use% and write to stdout
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # The Use% provided by df is not adedquate, being an integer
    fs_used_percent=$(calculate "(100*$fs_used_bytes)/($fs_used_bytes+$fs_avail_bytes)" 4 2DP)
    echo "$fs_used_percent"
    #msg D "get_fs_usage: fs_used_bytes: $fs_used_bytes, fs_avail_bytes: $fs_avail_bytes, fs_used_percent: $fs_used_percent"

}  # end of function get_fs_usage

#--------------------------
# Name: get_y_max
# Purpose: gets the maximum y value from the results file for the given column
# Outputs: maximum y value written to stdout
# Arguments:
#   $1: the file name of the report with the data
#   $2: the column number of the data
#--------------------------
function get_y_max {
    #fct "${FUNCNAME[0]}" 'started'
    local col_no=$2 regex report_fn=$1 y y_max

    y_max=0
    while read -r line
    do
        [[ $line =~ ^# ]] && continue        # Skip comments
        regex='^"'
        [[ $line =~ $regex ]] && continue    # Skip column headers
        #msg D "get_y_max: col_no: $col_no, line: $line"
        y=$(echo "$line" | awk "{print \$$col_no}")
        [[ $y = - ]] && continue             # Skip null values
        #msg D "get_y_max: y: $y, y_max: $y_max"
        (($(echo "scale=$bc_scale; $y > $y_max" | bc))) && y_max=$y
    done < "$report_fn"
    echo -e "$y_max"

    #fct "${FUNCNAME[0]}" 'returning'
}  # end of function get_y_max

#--------------------------
# Name: initialise
# Purpose: sets up environment and parses command line
#--------------------------
function initialise {
    local buf emsg regex startup_message
    local opt_l_flag opt_m_flag

    # Local variables required for the usage function
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    local -r \
        default_file_size_max=2 \
        default_file_size_min=0 \
        default_k=25 \
        default_target_use_percent=75.0 \
        default_target_use_percent_increment=4.0 \
        default_target_use_percent_max=98.0 \
        default_test_repeat_count=3 \

    # Configure shell environment
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~
    export PATH=/usr/sbin:/sbin:/usr/bin:/bin
    IFS=$' \n\t'
    set -o nounset
    shopt -s extglob            # Enable extended pattern matching operators
    unset CDPATH                # Ensure cd behaves as expected
    umask 0022
 
    # Set some global variables
    # ~~~~~~~~~~~~~~~~~~~~~~~~~
    readonly false=
    readonly true=true
    finalising_flag=$false
    global_error_flag=$false
    global_warning_flag=$false
    pid_file_created_flag=$false
    tmp_dir_created_flag= $false

    bc_scale=5                  # scale used with bc for Use% calculations
    pid_dir=/var/run
    script_name=${0##*/}        # Script name without path

    readonly target_use_percent_max_max=99.999
    readonly regex_uint='^[[:digit:]]+$'

    # Set defaults that may be overriden by command line parsing
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    debugging_flag=$false
    file_size_max=$default_file_size_max
    file_size_min=$default_file_size_min
    k=$default_k
    output_dir=$PWD
    target_use_percent=$default_target_use_percent
    target_use_percent_increment=$default_target_use_percent_increment
    target_use_percent_max=$default_target_use_percent_max
    test_repeat_count=$default_test_repeat_count
    
    # Parse command line
    # ~~~~~~~~~~~~~~~~~~
    emsg=
    opt_l_flag=$false
    opt_m_flag=$false
    while getopts :V:dhk:l:m:O:o:r:s:S:u:U:v: opt "${@:-}"
    do
        if [[ ${OPTARG:-} = -* ]]; then
            emsg=$emsg$'\n'"  Option $opt must have an argument"
            ((OPTIND--))
            continue
        fi
        case $opt in
            V )
                echo "$script_name version $script_ver"
                exit 0
                ;;
            d )
                debugging_flag=$true
                ;;
            h )
                debugging_flag=$false
                usage verbose \
                    $default_file_size_max \
                    $default_file_size_min \
                    $default_k \
                    $default_target_use_percent \
                    $default_target_use_percent_increment \
                    $default_target_use_percent_max \
                    $default_test_repeat_count
                exit 0
                ;;
            k )
                k=$OPTARG
                ;;
            l )
                log_dir=${OPTARG%%+(/)}    # Strip any trailing / chars
                opt_l_flag=$true
                ;;
            m )
                opt_m_flag=$true
                mountpoint=$OPTARG
                ;;
            o )
                output_dir=$OPTARG
                ;;
            r )
                test_repeat_count=$OPTARG
                ;;
            s )
                file_size_min=$OPTARG
                ;;
            S )
                file_size_max=$OPTARG
                ;;
            u )
                target_use_percent=$OPTARG
                ;;
            U )
                target_use_percent_increment=$OPTARG
                ;;
            v )
                target_use_percent_max=$OPTARG
                ;;
            * )
                emsg=$emsg$'\n'"  Invalid option '$OPTARG'"
        esac
    done

    # Set up logging if requested
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~
    if [[ $opt_l_flag ]]; then
        buf=$(ck_file "$log_dir" d:rwx: 2>&1)
        if [[ $buf = '' ]]; then
            log_fn=$log_dir/$script_name.$(date +%F@%T).log
            buf=$(touch "$log_fn" 2>&1)
            if (($? != 0)); then
                echo "$script_name: cannot create $log_fn: $buf" >&2
                exit 1
            fi
            logging_flag=$true
            exec 3>>"$log_fn"
            exec 4>>"$log_fn"
        else
            emsg=$emsg$'\n'"  option -l: log directory problem: $buf"
        fi
    else
        exec 3>/dev/tty
        exec 4>/dev/tty
    fi

    # Up to this point any messages have been given using echo followed by exit 1.  Now 
    # the essentials for msg() and finalise() have been established, all future messages 
    # will be sent using msg() which will call finalise() when called with an error message.

    fct "${FUNCNAME[0]}" 'started (this message delayed until logging initialised)'
    startup_message="$script_name version $script_ver started with command line '$*'"
    msg I "$startup_message"
    
    # Set traps
    # ~~~~~~~~~
    trap 'finalise 129' 'HUP'
    trap 'finalise 130' 'INT'
    trap 'finalise 131' 'QUIT'
    trap 'finalise 143' 'TERM'
    # In case a function called by command substitution calls finalise
    trap 'exit 1' 'USR1'

    # Test for mandatory options not set
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    if [[ ! $opt_m_flag ]]; then
        emsg=$'\n'"  Mandatory option -m not given"
    fi

    # Test for extra arguments
    # ~~~~~~~~~~~~~~~~~~~~~~~~
    shift $(($OPTIND-1))
    if [[ $* != '' ]]; then
        emsg=$emsg$'\n'"  Invalid extra argument(s) '$*'"
    fi

    # Check the mountpoint
    # ~~~~~~~~~~~~~~~~~~~~
    if [[ $opt_m_flag ]]; then
        buf=$(ck_file "$mountpoint" d:rwx: 2>&1)
        [[ $buf != '' ]] && emsg=$emsg$'\n'"  $buf"
        regex=$mountpoint$
        buf=$(df "$mountpoint" 2>&1)
        if [[ ! $buf =~ $regex ]]; then
            emsg=$emsg$'\n'"   $mountpoint is not a mountpoint"
        fi
    fi

    # Check values that must be positive integers/floats
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    if [[ ! $k =~ ^[0-9]+$ ]]; then
        emsg=$emsg$'\n'"  -k option value is not a positive integer: $k"
    fi  
    if [[ ! $file_size_min =~ ^[0-9]+$ ]]; then
        emsg=$emsg$'\n'"  -s option value is not a positive integer: $file_size_min"
    fi  
    if [[ ! $file_size_max =~ ^[0-9]+$ ]]; then
        emsg=$emsg$'\n'"  -S option value is not a positive integer: $file_size_max"
    fi  
    [[ $target_use_percent =~ ^[0-9]+$ ]] && target_use_percent=$target_use_percent.0
    if [[ ! $target_use_percent =~ ^[0-9]+\.[0-9]+$ ]]; then
        emsg=$emsg$'\n'"  -u option value is not a simple positive floating point number: $target_use_percent"
    elif (($(echo "scale=$bc_scale; $target_use_percent > $target_use_percent_max_max" | bc))); then
        emsg=$emsg$'\n'"  -u option value is greater than $target_use_percent_max_max: $target_use_percent"
    fi
    [[ $target_use_percent_increment =~ ^[0-9]+$ ]] && target_use_percent_increment=$target_use_percent_increment.0
    if [[ ! $target_use_percent_increment =~ ^[0-9]+\.[0-9]+$ ]]; then
        emsg=$emsg$'\n'"  -U option value is not a simple positive floating point number: $target_use_percent_increment"
    fi  
    [[ $target_use_percent_max =~ ^[0-9]+$ ]] && target_use_percent_max=$target_use_percent_max.0
    if [[ ! $target_use_percent_max =~ ^[0-9]+\.[0-9]+$ ]]; then
        emsg=$emsg$'\n'"  -v option value is not a simple positive floating point number: $target_use_percent_max"
    elif (($(echo "scale=$bc_scale; $target_use_percent_max > $target_use_percent_max_max" | bc))); then
        emsg=$emsg$'\n'"  -v option value is greater than $target_use_percent_max_max: $target_use_percent_max"
    fi
    if [[ $test_repeat_count =~ ^[0-9]+$ ]]; then
        ((test_repeat_count<2)) && \
            emsg=$emsg$'\n'"  -r option value is less than 2: $test_repeat_count"
    else
        emsg=$emsg$'\n'"  -r option value is not a positive integer: $test_repeat_count"
    fi  

    # Ensure the output directory is useable
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    cd "$output_dir" 2>/dev/null
    if (($?==0)); then
        output_dir=$PWD
        buf=$(ck_file "$output_dir" d:wx: 2>&1)
        [[ $buf != '' ]] && emsg=$emsg$'\n'"  -o (output directory): $buf" 
    else
        emsg=$emsg$'\n'"  Output directory: $(cd "$output_dir" 2>&1 | sed 's/.*cd: //')"
    fi
    
    # Report any errors
    # ~~~~~~~~~~~~~~~~~
    if [[ $emsg != '' ]]; then
        msg E "$emsg"
    fi
    
    # Take lock
    # ~~~~~~~~~
    buf=$(ck_file "$pid_dir" d:rwx: 2>&1)
    [[ $buf != '' ]] && msg E "PID directory: $buf"
    do_pid "${@:-}"

    # Set up some size-in-bytes variables
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # * Use bc for calculations because 2 GiB in bytes exceeds bash's 64-bit
    #   integer capacity.
    # * Use floats in bc and then bash's printf to round to the nearest integer
    #   because using "scale=0" in bc results in all the calculations being
    #   done as integer calculations, not just the final rounding.
    # Convert file_size_max from GiB to bytes
    file_size_max=$(calculate "$file_size_max*1073741824" 0 0DP)
    # Calculate the size of the range of file sizes
    file_size_range_size=$(calculate "$file_size_max-$file_size_min" 0 0DP)

    # Debugging summary of configuration values
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    msg D "
       file_size_max: $(printf "%'d" $file_size_max)
       file_size_min: $(printf "%'d" $file_size_min)
       file_size_range_size: $(printf "%'d" $file_size_range_size)
       k: $k
       mountpoint: $mountpoint
       output_dir: $output_dir
       target_use_percent: $target_use_percent
       target_use_percent_increment: $target_use_percent_increment
       target_use_percent_max: $target_use_percent_max
"
    # Make root of test file system the current directory
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    cd "$mountpoint" 2>/dev/null
    (($?>0)) && msg E "Unable to change directory to $mountpoint: $(cd "$mountpoint" 2>&1)"

    # Create temporary directory in root of test file system
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    buf=$(mktemp -d -p "$mountpoint" "$script_name.XXXXXX" 2>&1)
    if [[ $? -eq 0 ]]; then 
        tmp_dir=$buf
        tmp_dir_created_flag=$true
    else
        msg E "Unable to create temporary directory:$buf"
    fi

    # Initialise the report file
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~
    output_fn_timestamp=$(date +%d-%b-%Y@%R)
    initialise_report "$startup_message"

    # Get file system data
    # ~~~~~~~~~~~~~~~~~~~~
    get_file_system_data
    write_to_report -c $'File system\n~~~~~~~~~~~\n'"$fs_data"$'\n'
    write_to_report -c $'Partition\n~~~~~~~~~\n'"$part_data"$'\n'
    write_to_report -c $'Storage device\n~~~~~~~~~~~~~~\n'"$hdd_dev_data"$'\n'

    # Initialise the test arrays
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~
    test_idx=0
    file_or_dir_count=10000
    pretty_file_or_dir_count=$(printf "%'d" $file_or_dir_count)

    # test_name[]             Descriptive string.  Required.
    # test_avail_req[]        Space required for test in bytes.  Required if not
    #                         zero.
    # test_prep_cmd[]         The command to prepare for the test.  Required if
    #                         not empty.
    # test_prep_seq_count[]   The number of times to repeat the command to
    #                         prepare for the test. Required if nota one.
    # test_cmd[]              The command to run and time for the test.
    # test_seq_count[]        The number of times to run the test command.
    # test_cmd_stderr_regex[] A regex to match expected stderr from the test
    #                         command.  Required when stderr is not empty.
    # The last index must be [test_idx++].

    test_name[test_idx]="Create $pretty_file_or_dir_count directories"
    test_avail_req[test_idx]=$(calculate "$file_or_dir_count*$fs_inode_size" 0 0DP)
    test_seq_count[test_idx]=$file_or_dir_count
    test_cmd[test_idx++]='xargs mkdir'

    test_name[test_idx]="find $pretty_file_or_dir_count directories"
    test_avail_req[test_idx]=$(calculate "$file_or_dir_count*$fs_inode_size" 0 0DP)
    test_prep_cmd[test_idx]='xargs mkdir'
    test_prep_seq_count[test_idx]=$file_or_dir_count
    test_cmd[test_idx++]='find'

    test_name[test_idx]="find and remove $pretty_file_or_dir_count directories"
    test_avail_req[test_idx]=$(calculate "$file_or_dir_count*$fs_inode_size" 0 0DP)
    test_prep_cmd[test_idx]='xargs mkdir'
    test_prep_seq_count[test_idx]=$file_or_dir_count
    test_cmd[test_idx++]='find -type d -mindepth 1 -exec rmdir {} +'

    test_name[test_idx]="Create $pretty_file_or_dir_count files"
    test_avail_req[test_idx]=$(calculate "$file_or_dir_count*$fs_inode_size" 0 0DP)
    test_seq_count[test_idx]=$file_or_dir_count
    test_cmd[test_idx++]='xargs touch'

    test_name[test_idx]="find $pretty_file_or_dir_count files"
    test_avail_req[test_idx]=$(calculate "$file_or_dir_count*$fs_inode_size" 0 0DP)
    test_prep_cmd[test_idx]='xargs touch'
    test_prep_seq_count[test_idx]=$file_or_dir_count
    test_cmd[test_idx++]='find'

    test_name[test_idx]="find and remove $pretty_file_or_dir_count files"
    test_avail_req[test_idx]=$(calculate "$file_or_dir_count*$fs_inode_size" 0 0DP)
    test_prep_cmd[test_idx]='xargs touch'
    test_prep_seq_count[test_idx]=$file_or_dir_count
    test_cmd[test_idx++]='find -type f -exec rm {} +'

    test_name[test_idx]="Create 0.5 GiB file"
    test_avail_req[test_idx]=536870912
    test_cmd[test_idx]='dd if=/dev/zero of=0.5_GiB bs=1M count=512 status=noxfer'
    test_cmd_stderr_regex[test_idx++]='^512\+0 records in.512+\+0 records out$'

    test_name[test_idx]="Read 0.5 GiB file"
    test_cmd[test_idx]='dd if=../0.5_GiB of=/dev/null bs=1M count=512 status=noxfer'
    test_cmd_stderr_regex[test_idx++]='^512\+0 records in.512+\+0 records out$'

    test_name[test_idx]="Copy 0.5 GiB file into 1000 byte pieces"
    test_avail_req[test_idx]=$(calculate " \
            ($fs_inode_size+((512*1024*1024)/$fs_blocksize)) \
            + \
            (1000*($fs_inode_size+(1000/$fs_blocksize))) \
        " \
        0 0DP)
    test_cmd[test_idx++]='split --bytes=1000 ../0.5_GiB .'

    test_name[test_idx]="Copy 0.5 GiB file into 1024 byte pieces"
    test_avail_req[test_idx]=$(calculate " \
            ($fs_inode_size+((512*1024*1024)/$fs_blocksize)) \
            + \
            (1000*($fs_inode_size+(1024/$fs_blocksize))) \
        " \
        0 0DP)
    test_cmd[test_idx]='split --bytes=1024 ../0.5_GiB .'

    # Populate name-to-index array
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # @@@ is this used?
    for ((i=0;i<${#test_name[*]};i++))
    do
        report_col_idx_by_name[${test_name[i]}]=$i
    done
    
    fct "${FUNCNAME[0]}" 'returning'
}  # end of function initialise

#--------------------------
# Name: initialise_report
# Purpose: initialises the report file
# Arguments: $1: the script's version and options
#--------------------------
function initialise_report {
    fct "${FUNCNAME[0]}" 'started'
    local i

    report_fn=$output_dir/${script_name%.sh}.$output_fn_timestamp.report

    # Initial comments
    # ~~~~~~~~~~~~~~~~
    write_to_report -c "$(date)"$'\n\n'"$1"$'\n'

    fct "${FUNCNAME[0]}" 'returning'
}  # end of function initialise_report

#--------------------------
# Name: msg
# Purpose: generalised messaging interface
# Usage: msg class msg_text
#    class: must be one of D, E, I or W indicating Debug, Error, Information or Warning
#    msg_text: is the text of the message
# Output: information messages to fd3; the rest to fd4
# Return code: always zero
#--------------------------
function msg {
    local class message_text prefix

    class="${1:-}"
    case "$class" in 
        D ) 
            [[ ! $debugging_flag ]] && return
            prefix='DEBUG: '
            ;;
        E ) 
            global_error_flag=$true
            prefix='ERROR: '
            ;;
        I ) 
            prefix=
            ;;
        W ) 
            global_warning_flag=$true
            prefix='WARN: '
            ;;
        * )
            msg E "$my_nam: msg: invalid class '$class': '$*'"
            class=I
    esac
    message_text="${2:-}"

    if [[ $class = I ]]; then
        echo "$(date '+%H:%M:%S ')$prefix$message_text" >&3
    else
        echo "$(date '+%H:%M:%S ')$prefix$message_text" >&4
        [[ $class = E ]] && finalise 1 
    fi

    return 0
}  #  end of function msg

#--------------------------
# Name: run_tests
# Purpose: loops, filling the file system to increasing levels and running tests
#--------------------------
function run_tests {
    fct "${FUNCNAME[0]}" 'started'
    local buf last_pass_flag OK_flag real_t report_line result
    local test_working_dir test_stderr time_out time_out_regex use_percent

    # Initialise the regex for output from the time keyword
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # Sample od -a output of time output
    # 0000000  nl   r   e   a   l  ht   0   m   0   .   0   0   2   s  nl   u
    # 0000020   s   e   r  ht   0   m   0   .   0   0   1   s  nl   s   y   s
    # 0000040  ht   0   m   0   .   0   0   0   s
    buf=$'\t''[[:digit:]]+m[[:digit:]\.]+s'
    time_out_regex=$'^\n'"real${buf}"$'\n'"user${buf}"$'\n'"sys${buf}\$"

    # Test the raw sequential read of the device the file system is on
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    test_raw_sequential_read
    write_to_report -c "Raw sequential read rate    : $raw_sequential_read_rate MB/s"$'\n'

    # Write data column headers to the report file
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    buf=
    for ((i=0;i<${#test_name[*]};i++))
    do
        buf="$buf \"${test_name[i]}\""
    done
    buf=${buf# }
    write_to_report "$buf"

    # Create a 0.5 GiB file for use in some of the tests
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    cd "$tmp_dir" 2>/dev/null
    (($?>0)) && msg E "Unable to cd: $(cd "$tmp_dir" 2>&1)"
    buf=$(dd if=/dev/zero of=0.5_GiB bs=1M count=512 2>&1)
    (($?>0)) && msg E "Unable to create 0.5 GiB file: $buf"

    # Do nothing more if the file system usage is too high
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    get_fs_usage > /dev/null
    if (($(echo "scale=$bc_scale; $fs_used_percent < $target_use_percent" | bc))); then
        msg I "Initial Use% is $fs_used_percent%"
    else
        msg E "Use% ($fs_used_percent) is already >= the target Use% ($target_use_percent)"
    fi

    # Run tests until the file system's %Use is at the maximum target level
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    last_pass_flag=$false
    test_stderr=$tmp_dir/test_stderr
    test_working_dir=$tmp_dir/d
    while (($(echo "scale=$bc_scale; $(get_fs_usage) < $target_use_percent_max" | bc )))
    do
        # Fill the file system to the next target Use%
        # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        fill_to_level
        report_line=$(get_fs_usage)

        # For each test
        # ~~~~~~~~~~~~~
        # TODO: move body of this loop into a function
        for ((i=0;i<${#test_name[*]};i++))
        do
            break_flag=$false

            # Ensure enough space
            # ~~~~~~~~~~~~~~~~~~~
            if ((${test_avail_req[i]:-0}>fs_avail_bytes)); then
                msg W "Not enough space to run test \"${test_name[i]}\""
                report_line+=' -'
                continue
            fi
            msg D "Running test \"${test_name[i]}\""

            # Run the test the required number of times
            # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            for ((j=0;j<$test_repeat_count;j++))
            do
                # Make the test working directory current
                # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
                mkdir -p "$test_working_dir" 2>/dev/null
                cd "$test_working_dir" 2>/dev/null
                (($?>0)) && msg E "$(cd "$test_working_dir" 2>&1)"

                # Prepare for the test
                # ~~~~~~~~~~~~~~~~~~~~
                if [[ ${test_prep_cmd[i]:-} != '' ]]; then
                    if ((${test_prep_seq_count[i]:-0}==0)); then
                        buf=$(${test_prep_cmd[i]} 2>&1)
                        rc=$?
                    else
                        buf=$(seq 1 ${test_prep_seq_count[i]} | ${test_prep_cmd[i]} 2>&1)
                        rc=$?
                    fi
                    ((rc>0)) && msg E "Problem running test preparation command (${test_prep_cmd[i]}): $buf"
                fi
                flush_buffers

                # Run the test
                # ~~~~~~~~~~~~
                if ((${test_seq_count[i]:-0}==0)); then
                    buf=$({ time (${test_cmd[i]} >/dev/null 2>"$test_stderr" && flush_buffers); } 2>&1)
                else
                    buf=$({ time (seq 1 ${test_seq_count[i]} \
                       | ${test_cmd[i]} >/dev/null 2>"$test_stderr" && flush_buffers); } 2>&1 \
                    )
                fi
                [[ ! $buf =~ $time_out_regex ]] && msg E "Problem with test: $buf"
                time_out=$buf
                if [[ -s $test_stderr ]]; then
                    error_flag=$true
                    buf="$(cat "$test_stderr")"
                    #msg D "LINENO: $LINENO, buf: >>$buf<<"
                    #msg D "LINENO: $LINENO, test_cmd_stderr_regex[$i]: >>${test_cmd_stderr_regex[i]:-}<<"
                    [[ ${test_cmd_stderr_regex[i]:-} != '' ]] \
                        && [[ $buf =~ ${test_cmd_stderr_regex[i]} ]] \
                        && error_flag=$false
                    if [[ $error_flag ]]; then
                        msg W "Problem with test (stderr): $buf"
                        msg W "Aborting this test"
                        continue
                    fi
                fi
                read minutes seconds <<< $(echo $time_out | awk -v FS='m|s| ' '{ print $2 " " $3 }')
                result[j]=$(calculate "($minutes*60)+$seconds" 6 4SF)

                # Clean up
                # ~~~~~~~~
                cd "$tmp_dir" 2>/dev/null
                (($?>0)) && msg E "Unable to cd: $(cd "$tmp_dir" 2>&1)"
                rm -fr "$test_working_dir"
            done

            # Analyse the results
            # ~~~~~~~~~~~~~~~~~~~
            #result=$(IFS=+; calculate "$bc_scale" "(${result[*]})/$test_repeat_count")
            buf=$(echo ${result[*]} | awk '{
                # Calculate the mean
                for (n=1; n <= NF; n++) {
                    #print "n: " n ", $n: " $n
                    sum += $n;
                }
                mean = sum / NF
                #print "mean: " mean

                # Normalise the numbers (to a mean of one)
                # Calculate the variance of the normalised numbers
                # Calculate the standard deviation, sigma
                for (n=1; n <= NF; n++) {
                    $n = $n / mean
                    #print "Normalised n: " n ", $n: " $n
                    sum2 += ($n - 1) ^ 2
                    #print "n: " n ", sum2: " sum2
                }
                variance = sum2 / NF
                sigma = sqrt( variance )
           
                print "mean:" mean " sigma:" sigma
            }' -)
            mean=${buf#mean:}
            mean=${mean% *}
            printf -v mean "%.3"g "$mean"      # Round to three significant figures
            sigma=${buf#*sigma:}
            printf -v sigma "%.2"g "$sigma"    # Round to two significant figures
            report_line+=" $result"
            msg D "Raw results: $(echo ${result[*]}). Result (mean): $mean. Standard deviation of normalised results: $sigma"

        done

        # Write results to report
        # ~~~~~~~~~~~~~~~~~~~~~~~
        write_to_report "$report_line"

        # Increment the target Use%
        # ~~~~~~~~~~~~~~~~~~~~~~~~~
        [[ $last_pass_flag ]] && break    # Already maxxed out
        msg D "run_tests: incrementing target Use%: $target_use_percent + $target_use_percent_increment"
        target_use_percent=$(calculate "$target_use_percent+$target_use_percent_increment" 4 2DP)
        #echo "DEBUG: scale=$bc_scale; $target_use_percent > $target_use_percent_max"
        if (($(echo "scale=$bc_scale; $target_use_percent > $target_use_percent_max" | bc))); then
            target_use_percent=$target_use_percent_max
            last_pass_flag=$true
        fi
        msg D "target_use_percent after incrementing: $target_use_percent"
       
    done
    
    fct "${FUNCNAME[0]}" 'returning'
}  # end of function run_tests

#--------------------------
# Name: test_raw_sequential_read
# Purpose: runs sdparm -tT to test the raw sequential read rate
# Global variables:
#   * Sets raw_sequential_read_rate
#--------------------------
function test_raw_sequential_read {
    fct "${FUNCNAME[0]}" 'started'
    local buf i rc regex result

    raw_sequential_read_rate=    # Ensure set in case of skip or error

    # Skip the raw sequential read test?
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    if ((EUID!=0)); then
        msg W "Skipping the raw sequential read test; it can only be run by root"
        fct "${FUNCNAME[0]}" 'returning'
        return 1
    fi
    #echo "DEVEL: skipping raw sequential read test"; fct "${FUNCNAME[0]}" 'returning'; return

    # Run the raw sequential read test
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # hdparm's -T option is used to improve the accuracy of the -t option result
    # TODO: run the test until results are consistent, discarding any results 5% slower than the fastest
    msg I "Testing raw sequential read rate"
    for ((i=0;i<$test_repeat_count;i++))
    do
        buf=$(hdparm -tT "$hdd_dev" 2>&1)
        rc=$?
        if ((rc>0)); then
            msg W "Unable to run the raw sequential read test on $hdd_dev: $buf"
            fct "${FUNCNAME[0]}" 'returning'
            return 1
        fi
        read -r _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ result[i] _ <<< $buf
    done

    # Average the results
    # ~~~~~~~~~~~~~~~~~~~
    # Two decimal places used to match hdparm's output
    oIFS=$IFS
    export IFS=+
    raw_sequential_read_rate=$(calculate "(${result[*]})/$test_repeat_count" 4 2DP)
    export IFS=$oIFS
    
    fct "${FUNCNAME[0]}" 'returning'
}  # end of function test_raw_sequential_read

#--------------------------
# Name: usage
# Purpose: prints usage message
#--------------------------
function usage {
    fct "${FUNCNAME[0]}" 'started'
    local \
        default_file_size_max \
        default_file_size_min \
        default_k \
        default_report_fn \
        default_target_use_percent \
        default_target_use_percent_increment \
        default_target_use_percent_max \
        default_test_repeat_count

    echo "usage: $script_name [-V] [-d] [-h] [-k constant] -m mountpoint
    [-s size_min] [-S size_max] [-u Use%] [-U Use%] [-v Use%]" >&2    
    if [[ ${1:-} != 'verbose' ]]; then
        echo "(use -h for help)" >&2
    else
        default_file_size_max=$2
        default_file_size_min=$3
        default_k=$4
        default_target_use_percent=$5
        default_target_use_percent_increment=$6
        default_target_use_percent_max=$7
        default_test_repeat_count=$8

        echo "  where:
    -V prints the script's version and exits.
    -d turns debugging trace on.
    -h prints this help and exits.
    -k a constant determining the mean proportions of small and big files, default $default_k
    -l names the directory for the log file.
       If not given, will log to /dev/tty
       If given, a log will be created in the named directory called
           $script_name.<time stamp>.log
       The <time stamp> will be the output of \"date +%F@%T\".
    -m names the mountpoint of the file system to be populated.
    -o the output directory, default the current directory.
    -r the test repeat count, default $default_test_repeat_count, minimum 2.
    -s the minimum size in bytes of file to be created, default $default_file_size_min.
    -S the maximum size in GiB of file to be created, default $default_file_size_max
    -u the initial target Use% of the file system, default $default_target_use_percent.
    -U the target Use% increment, default $default_target_use_percent_increment.
    -v the final target Use% of the file system.  Maximum accepted value $target_use_percent_max_max.
       Default $default_target_use_percent_max.

       Notes: 
       * Use% is calculated as \"used/(used+available)\", the same as df does but with
         a fractional part.
       * Use% values (-u, -U and -v values) may be given with a decimal fractional part.
       * Use% calculations are done using floats with $bc_scale decimal points.
" >&2
    fi

    fct "${FUNCNAME[0]}" 'returning'
}  # end of function usage

#--------------------------
# Name: write_gnuplot_scripts
# Purpose: writes gnuplot script files to display the data in the report
# Global variables read:
#     fs_type
#     output_dir
#     output_fn_timestamp
#     part_data
#     report_fn
#     script_name
#     test_name[]
#--------------------------
function write_gnuplot_scripts {
    fct "${FUNCNAME[0]}" 'started'
    local my_test_name part_size title y_col y_max

    # Get key file system data to display on plots
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    part_size=$(echo "$part_data" | grep '^Partition size: ')

    # For each test
    # ~~~~~~~~~~~~~
    for ((i=0;i<${#test_name[*]};i++))
    do
        my_test_name=${test_name[i]}
        msg D "Writing gnuplot script for \"$my_test_name\""
        gnuplot_script_fn=$output_dir/$output_fn_timestamp.$my_test_name.gnuplot_script.$i

        # Calculate maximum y value to display
        # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        y_col=$((i+2))
        y_max=$(get_y_max "$report_fn" $y_col)
        msg D "write_gnuplot_scripts: y_max: $y_max"
        if [[ ! $y_max =~ (^[0-9]+(\.[0-9]+)?$)|(^\.[0-9]+$) ]]; then
            msg W "Skipping gnuplot script for $my_test_name; y_max ($y_max) is not a number"
            continue
        fi
        # Add 10% headroom
        y_max=$(calculate "$y_max*1.1" 5 3SF)    # Calculate with 2 more dp than bash time output 

        # Generate title string
        # ~~~~~~~~~~~~~~~~~~~~~
        title="\"$my_test_name\\n$fs_type\\n$part_size\""
        echo "# gnuplot script file
#
# Automatically generated by $script_name to plot $my_test_name results.
#
# The intended results file is $report_fn
#
# Ensure known (default) gnuplot settings
clear
reset
unset key
#
set title $title
set xlabel \"File system usage (%)\"
set xrange [0:100]
set yrange [0:$y_max]
set ylabel \"Test time (seconds)\"
plot '$report_fn' using 1:$y_col with lines linewidth 2 linetype 1 linecolor -1 smooth unique, \
   '$report_fn' using 1:$y_col with points linewidth 1 linecolor 9 pointtype 28 pointsize 1
" > "$gnuplot_script_fn"

    done

    fct "${FUNCNAME[0]}" 'returning'
}  # end of function write_gnuplot_scripts

#--------------------------
# Name: write_to_report
# Purpose: writes (appends) arguments to the report file
# Arguments:
#    $1 may be -c in which case the data is commented by prefixing each line with #
#--------------------------
function write_to_report {
    #fct "${FUNCNAME[0]}" 'started'
    local buf out_buf

    out_buf=
    if [[ ${1:-} = -c ]]; then
        shift
        while read -r line
        do
            out_buf=$out_buf$'\n'"# $line"
        done < <(echo "${@:-}")
        out_buf=${out_buf#$'\n'}
    else
        out_buf="$*"
    fi
    buf=$(echo -e "$out_buf" 2>&1 >>"$report_fn")
    [[ $buf != '' ]] && msg E "Problem writing to report file ($report_fn): $buf"
    #echo "DEBUG: report after writing a line:"$'\n'"$(cat "$report_fn")"

    #fct "${FUNCNAME[0]}" 'returning'
}  # end of function write_to_report

#--------------------------
# Name: main
# Purpose: where it all happens
#--------------------------
declare -A report_col_idx_by_name    # Declared here in case bash < 4.2 so no -g option
initialise "${@:-}"
run_tests
write_gnuplot_scripts
finalise 0
